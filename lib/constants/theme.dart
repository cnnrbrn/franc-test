import 'package:flutter/material.dart';

const Color primaryColor = Color(0xffFF673C);
const Color primaryGradient = Color(0xffff8a3c);

ThemeData theme() {
  final ThemeData base = ThemeData.light();

  return base.copyWith(
      primaryColor: primaryColor,
      accentColor: Color(0xff22d9a1),
      primaryTextTheme:
          TextTheme(headline6: TextStyle(color: Color(0xff0074c9))),
      appBarTheme: AppBarTheme(
          brightness: Brightness.light,
          elevation: 0,
          color: Colors.white,
          iconTheme: IconThemeData(color: Color(0xff0074c9))),
      // backgroundColor: Color(0xffFF673C),
      floatingActionButtonTheme: FloatingActionButtonThemeData(
          backgroundColor: primaryColor,
          splashColor: Colors.white.withOpacity(0.25),
          elevation: 2),
      buttonTheme: ButtonThemeData(
          buttonColor: Color(0xff0074c9), textTheme: ButtonTextTheme.primary));
}
