import 'package:flutter/material.dart';
import 'package:franc_test/screens/deposit/depositScreen.dart';
import 'package:franc_test/screens/home/homeScreen.dart';

import 'constants/theme.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Franc Test',
      theme: theme(),
      home: HomeScreen(),
      routes: {
        "/deposit": (context) => DepositScreen(),
      },
    );
  }
}
