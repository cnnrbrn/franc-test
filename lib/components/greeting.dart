import 'package:flutter/material.dart';

class Greeting extends StatelessWidget {
  const Greeting({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text("Hey there",
              style: TextStyle(color: Colors.grey[500], fontSize: 18)),
          Text("Jane Doe",
              style: TextStyle(
                  color: Colors.grey[800],
                  fontSize: 26,
                  fontWeight: FontWeight.bold))
        ]),
      ),
    );
  }
}
