import 'package:flutter/material.dart';

class FIcon extends StatelessWidget {
  final double size;
  final bool hasShadow;

  const FIcon({Key key, this.size, this.hasShadow}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: size,
      height: size,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        image: DecorationImage(image: AssetImage('images/icon1.png')),
        boxShadow: hasShadow != null
            ? [
                BoxShadow(
                  color: Colors.black.withOpacity(0.07),
                  spreadRadius: 3,
                  blurRadius: 7,
                  offset: Offset(0, 6),
                ),
              ]
            : null,
      ),
    );
  }
}
