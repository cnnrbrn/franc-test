import 'package:flutter/material.dart';
import 'package:franc_test/constants/theme.dart';

import '../icon.dart';

class PrimaryCard extends StatelessWidget {
  final String title;
  final Function onTap;

  const PrimaryCard({Key key, this.title, this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50.0),
      child: Stack(clipBehavior: Clip.none, children: <Widget>[
        InkWell(
          onTap: onTap,
          child: Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [primaryGradient, Theme.of(context).primaryColor]),
              borderRadius: BorderRadius.all(Radius.circular(28)),
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.3),
                  spreadRadius: 3,
                  blurRadius: 9,
                  offset: Offset(0, 7),
                ),
              ],
            ),
            padding: EdgeInsets.fromLTRB(30, 60, 30, 30),
            child: SizedBox(
                height: 100,
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(title,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.w800,
                        )),
                  ],
                )),
          ),
        ),
        Positioned(
            child: Hero(
                tag: "icon",
                child: FIcon(
                  size: 80,
                  hasShadow: true,
                )),
            left: 20,
            top: -40),
      ]),
    );
  }
}
