import 'package:flutter/material.dart';

import '../icon.dart';

class SecondaryCard extends StatelessWidget {
  final String title;

  const SecondaryCard({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50.0),
      child: Stack(clipBehavior: Clip.none, children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(28)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.1),
                spreadRadius: 2,
                blurRadius: 5,
                offset: Offset(0, 1),
              ),
            ],
          ),
          padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
          child: SizedBox(
              width: double.infinity,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  FIcon(size: 80),
                  SizedBox(height: 20),
                  Text(title,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w800,
                      )),
                ],
              )),
        ),
      ]),
    );
  }
}
