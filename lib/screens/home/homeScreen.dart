import 'package:flutter/material.dart';
import 'package:franc_test/components/cards/primaryCard.dart';
import 'package:franc_test/components/cards/secondaryCard.dart';
import 'package:franc_test/components/greeting.dart';

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[100],
      appBar: AppBar(
        title: null,
        backgroundColor: Colors.grey[100],
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Greeting(),
              PrimaryCard(
                  title: "House Deposit",
                  onTap: () {
                    Navigator.pushNamed(context, "/deposit");
                  }),
              SecondaryCard(
                title: "Risk Assessment",
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        tooltip: 'New',
        child: Icon(Icons.add),
      ),
    );
  }
}
