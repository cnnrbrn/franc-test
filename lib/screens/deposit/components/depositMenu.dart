import 'package:flutter/material.dart';

class DepositMenu extends StatelessWidget {
  const DepositMenu({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.2),
              spreadRadius: 2,
              blurRadius: 7,
              offset: Offset(0, 5),
            ),
          ],
          borderRadius: BorderRadius.all(Radius.circular(18))),
      child: SizedBox(
        height: 80,
        width: MediaQuery.of(context).size.width - 20,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              child: Icon(
                Icons.cloud_download,
                color: Theme.of(context).accentColor,
                size: 24.0,
              ),
            ),
            Container(
              child: Icon(
                Icons.compare_arrows,
                color: Theme.of(context).accentColor,
                size: 24.0,
              ),
            ),
            Container(
              child: Icon(
                Icons.update,
                color: Theme.of(context).accentColor,
                size: 24.0,
              ),
            ),
            Container(
              child: Icon(
                Icons.edit,
                color: Theme.of(context).accentColor,
                size: 24.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
