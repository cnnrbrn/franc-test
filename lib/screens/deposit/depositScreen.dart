import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:franc_test/components/icon.dart';
import 'package:franc_test/constants/theme.dart';

import 'components/depositMenu.dart';

class DepositScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarBrightness: Brightness.dark,
    ));

    return Container(
      color: primaryGradient,
      child: SafeArea(
        child: Scaffold(
          body: CustomScrollView(
            slivers: [
              SliverPersistentHeader(
                delegate:
                    MySliverAppBar(expandedHeight: 300, minimumHeight: 100),
                pinned: true,
              ),
              SliverPadding(
                padding: EdgeInsets.only(top: 45.0),
                sliver: SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (_, index) => ListTile(
                      title: Text("Item: $index"),
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class MySliverAppBar extends SliverPersistentHeaderDelegate {
  final double minimumHeight;
  final double expandedHeight;

  MySliverAppBar({@required this.minimumHeight, @required this.expandedHeight});

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        Container(
            decoration: BoxDecoration(
          color: Theme.of(context).primaryColor,
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [primaryGradient, Theme.of(context).primaryColor]),
        )),
        Positioned(
            top: 15,
            left: MediaQuery.of(context).size.width / 2 - 58,
            child: Opacity(
                opacity: max(0.0, shrinkOffset) / expandedHeight,
                child: Text(
                  "HOUSE DEPOSIT",
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ))),
        Padding(
          padding: EdgeInsets.fromLTRB(30, 60, 30, 10),
          child: Opacity(
            opacity: 1 - max(0.0, shrinkOffset) / expandedHeight,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Hero(tag: "icon", child: FIcon(size: 110)),
                SizedBox(height: 30),
                Text(
                  "House Deposit",
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                    fontSize: 23,
                  ),
                ),
              ],
            ),
          ),
        ),
        Positioned(
          top: max(55, expandedHeight - shrinkOffset - 50),
          left: 10,
          child: Opacity(
            opacity: 1,
            child: DepositMenu(),
          ),
        ),
        Align(
          alignment: Alignment.topLeft,
          child: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => minimumHeight;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) => true;
}
